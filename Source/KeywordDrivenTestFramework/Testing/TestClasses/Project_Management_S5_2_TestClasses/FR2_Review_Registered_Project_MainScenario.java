/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Project_Management_S5_2_TestClasses;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.Project_Management_V5_2_PageObject.Project_Management_PageObject;
import KeywordDrivenTestFramework.Entities.TestResult;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.apache.derby.client.am.Decimal;
import org.openqa.selenium.By;

/**
 *
 * @author LDisemelo
 */
@KeywordAnnotation(
        Keyword = "FR2 Review Registered Project",
        createNewBrowserInstance = false
)

public class FR2_Review_Registered_Project_MainScenario extends BaseClass {
    String error = "";

    public FR2_Review_Registered_Project_MainScenario() {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!BusinessUnitVerification()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("•	Project Management record is saved");
    }

    public boolean BusinessUnitVerification () {
        
        pause(2500);
        //Switch to Project Approval Tab
        
        SeleniumDriverInstance.scrollToElement(Project_Management_PageObject.projectApprovalTab());
        
//        if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.projectApprovalTab())){
//            error = "Failed to wait for the Project Approval Tab.";
//            return false;
//        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.projectApprovalTab())){
            error = "Failed to click the Project Approval Tab.";
            return false;
        }
        
        pause(2500);
        SeleniumDriverInstance.scrollToElement(Project_Management_PageObject.businesssUnit_Checkbox());
                
        //Tick Business unit verification and approval user Checkbox
        if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.businesssUnit_Checkbox())){
            error = "Failed to wait for the Tick Business unit verification and approval user Checkbox.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.businesssUnit_Checkbox())){
            error = "Failed to click the Tick Business unit verification and approval user Checkbox.";
            return false;
        }
        narrator.finalizeTest("Successfully ticked the Tick Business unit verification and approval user Checkbox");
         pause(5000);
        
        //Save Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.saveBtn())) {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.saveBtn())) {
            error = "Failed to click on 'Save' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the save button.");

        pause(6000);
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Project_Management_PageObject.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
//
//        //Validate if the record has been saved or not.
//        if (!SeleniumDriverInstance.waitForElementsByXpath(Project_Management_PageObject.validateSave())) {
//            error = "Failed to wait for Save validation.";
//            return false;
//        }
//        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Project_Management_PageObject.validateSave());
//
//        if (!SaveFloat.equals("Record saved")) {
//            narrator.stepPassedWithScreenShot("Failed to save record.");
//            return false;
//        }
//        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");

        return true;
    }
   
}
