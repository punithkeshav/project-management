/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Project_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Project_Management_V5_2_PageObject.Project_Management_PageObject;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author LDisemelo
 */

@KeywordAnnotation(
        Keyword = "Edit Project Management",
        createNewBrowserInstance = false
)
public class FR9_Edit_Project_Management_MainScenario extends BaseClass {
    String error = "";
    
    public FR9_Edit_Project_Management_MainScenario()
    {
    //Navigate to Project Management
              Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        
    
    }
    
      public TestResult executeTest() {
        if (!Navigate_To_Project_Management()) {
            return narrator.testFailed("Failed due - " + error);
        }
        
      if (!Edit_Project_Management()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Project Management record is saved");
       }
       
        public boolean Navigate_To_Project_Management() {
        
        //Project Management
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.project_Management())) {
            error = "Failed to wait for Project Managenment Tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.project_Management())) {
            error = "Failed to click Projects Managenment Tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on Projects Managenment Tab");

   
        
        return true;
    }
        
        public boolean Edit_Project_Management(){
           //Navigate to search
            pause(5000);
       if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.searchFilter())) {
            error = "Failed to wait for search button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.searchFilter())) {
            error = "Failed to click search button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on search button");
        
        
       pause(7200);
       
       //Select A record
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.selectRecord())) {
            error = "Failed to wait to Select A record.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.selectRecord())) {
            error = "Failed to click Select A record.";
            return false;
        }
         narrator.stepPassedWithScreenShot("Successfully clicked on Select A record");
        pause(7200);
            
        // Navigate to Project actions
       if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.ProjectActions_tab())) {
            error = "Failed to wait for Project Actions Tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.ProjectActions_tab())) {
            error = "Failed to click Projects Actions Tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on Projects Actions Tab");
       
       pause(3500);
       
       
       //Add
       
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.ProjectActions_Add_Button())) {
            error = "Failed to wait for Project Actions Tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.ProjectActions_Add_Button())) {
            error = "Failed to click Projects Actions Tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on Projects Actions Add button");
        
        pause(4500);
       
      if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.actionsProcessFlow_Button())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.actionsProcessFlow_Button())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");  
        
        //Type of action
        
       if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.typeOfAction_dropdown())) {
            error = "Failed to wait for 'Type of action' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.typeOfAction_dropdown())) {
            error = "Failed to click on 'Type of action' dropdown.";
            return false;
        }
        
      if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Text2(getData("Type of action"))))
        {
            error = "Failed to wait for Type of action drop down option : " + getData("Type of action");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Text2(getData("Type of action"))))
        {
            error = "Failed to click Type of action drop down option : " + getData("Type of action");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Type of action: " + getData("Type of action"));
       
        
        //Action description
        
      if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.actionDescription())) {
            error = "Failed to wait for 'Action description.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.actionDescription(),getData("Action description"))) {
            error = "Failed to click on 'Type of action' dropdown.";
            return false;
        }
        
        //Entity
        
       if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.entity_dropdown())) {
            error = "Failed to wait for 'Entity' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.entity_dropdown())) {
            error = "Failed to click on 'Entity' dropdown.";
            return false;
        }
        
          if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.text_Search3(),(getData("Entity")))) {
            error = "Failed to click the Entity source: " + getData("Entity");
            return false;
        }
        
        if (!SeleniumDriverInstance.pressEnter())
        {
            error = "Failed to press enter";
            return false;
        }
        
        pause(3500);
        
      if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Text2(getData("Entity"))))
        {
            error = "Failed to wait for Entity1 source drop down option : " + getData("Entity");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Project_Management_PageObject.Text2(getData("Entity"))))
        { 
            error = "Failed to click Entity1 drop down option : " + getData("Entity1");
            return false;
        }
        
         if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Text2(getData("Entity1"))))
        {
            error = "Failed to wait for Entity1 source drop down option : " + getData("Entity1");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Project_Management_PageObject.Text2(getData("Entity1"))))
        {
            error = "Failed to click Entity1 drop down option : " + getData("Entity1");
            return false;
        }
        pause(2000);
        
        //Responsible person
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.responsiblePerson_dropdown())) {
            error = "Failed to wait for 'Responsible person' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.responsiblePerson_dropdown())) {
            error = "Failed to click on 'Responsible person' dropdown.";
            return false;
        }
        
          if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.text_Search2(),(getData("Responsible person")))) {
            error = "Failed to click the Entity source: " + getData("Responsible person");
            return false;
        }
        
        if (!SeleniumDriverInstance.pressEnter())
        {
            error = "Failed to press enter";
            return false;
        }
        
        pause(3500);
        
      if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Text2(getData("Responsible person1"))))
        {
            error = "Failed to wait for Responsible person source drop down option : " + getData("Responsible person1");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Project_Management_PageObject.Text2(getData("Responsible person1"))))
        {
            error = "Failed to click Responsible person drop down option : " + getData("Responsible person1");
            return false;
        }
        
        //Agency
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.agency_dropdown())) {
            error = "Failed to wait for 'Agency' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.agency_dropdown())) {
            error = "Failed to click on 'Agency' dropdown.";
            return false;
        }

        
        pause(3500);
        
      if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Text2(getData("Agency"))))
        {
            error = "Failed to wait for Agency drop down option : " + getData("Agency");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Project_Management_PageObject.Text2(getData("Agency"))))
        {
            error = "Failed to click Agency drop down option : " + getData("Agency");
            return false;
        }
        
        //Action DueDate
         if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.actionDueDate(), startDate + 10000)){
            error = "Failed to enter the planned start date";
            return false;
        }
        
         //Save Button
            if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.saveBtn2())) {
                error = "Failed to wait for 'Save' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.saveBtn2())) {
                error = "Failed to click on 'Save' button.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked the save button.");

            pause(8000);
            //Saving mask
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Project_Management_PageObject.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
            pause(3500);
            
            
            SeleniumDriverInstance.switchToFrameByXpath(Project_Management_PageObject.iframeXpath());
            
           // SeleniumDriverInstance.switchToTabOrWindow();
            
            
            //Are you sure want close
            
            
            //Close
            
           if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.projectClose())) {
                error = "Failed to click on 'Close' button.";
                return false;
            }
              if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.projectClose())) {
                error = "Failed to click on 'Close' button.";
                return false;
            }
       pause(9000);
//       
//       if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.projectClose1())) {
//                error = "Failed to click on 'Close' button.";
//                return false;
//            }
//              if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.projectClose1())) {
//                error = "Failed to click on 'Close' button.";
//                return false;
//            }
       //Select A record
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.selectRecord())) {
            error = "Failed to wait to Select A record.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.selectRecord())) {
            error = "Failed to click Select A record.";
            return false;
        }
          narrator.stepPassedWithScreenShot("Successfully clicked on Select A record");
        
        pause(8200);
            
        // Navigate to Project actions
       if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.ProjectActions_tab())) {
            error = "Failed to wait for Project Actions Tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.ProjectActions_tab())) {
            error = "Failed to click Projects Actions Tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on Projects Actions Tab");
       
       pause(3500);
       
       
            return true;
        }
}
