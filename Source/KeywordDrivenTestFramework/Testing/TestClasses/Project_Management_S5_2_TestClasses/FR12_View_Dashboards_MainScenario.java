/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Project_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Project_Management_V5_2_PageObject.Project_Management_PageObject;

/**
 *
 * @author LDisemelo
 */

@KeywordAnnotation(
        Keyword = "View Dashboards",
        createNewBrowserInstance = false
)
public class FR12_View_Dashboards_MainScenario extends BaseClass {
    String error = "";
    
    public FR12_View_Dashboards_MainScenario()
    {
    
    }
    
     public TestResult executeTest() {

      if (!View_Dashboards()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Viewed Dashboard");
       }
    public boolean View_Dashboards() {
        
        //ViewDasboard
        pause(3500);
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.viewDasboard())) {
            error = "Failed to wait for viewDasboard.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.viewDasboard())) {
            error = "Failed to click view Dasboard.";
            return false;
        }
  
   narrator.stepPassedWithScreenShot("Successfully clicked on View Dasboard");
   
      pause(9500);
      
      //navigate  button
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.navigateButton())) {
            error = "Failed to wait for view Dasboard.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.navigateButton())) {
            error = "Failed to click view Dasboard.";
            return false;
        }
      narrator.stepPassedWithScreenShot("Successfully clicked on navigate  button");
        
        //App OverView
           if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.appOverView())) {
            error = "Failed to wait for view App OverView";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.appOverView())) {
            error = "Failed to click view App OverView.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked on App OverView button");
        pause(5500);
   
     return true;
           }
}
