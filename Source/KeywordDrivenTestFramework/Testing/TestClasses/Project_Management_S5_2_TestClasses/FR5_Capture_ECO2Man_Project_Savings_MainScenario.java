/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Project_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Project_Management_V5_2_PageObject.Project_Management_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;

/**
 *
 * @author LDisemelo
 */
@KeywordAnnotation(
        Keyword = "FR5-Capture ECO2Man Project Savings",
        createNewBrowserInstance = false
)

public class FR5_Capture_ECO2Man_Project_Savings_MainScenario extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR5_Capture_ECO2Man_Project_Savings_MainScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest() {
        if (!ECO2Man_Project_Savings()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Project Management Eco2Man Project Savings  Rebates  Efficiencies record is saved.");
    }

    public boolean ECO2Man_Project_Savings() {
        //Navigate to ECO2Man Tab
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.eco2Man_Tab())) {
            error = "Failed to wait for 'ECO2Man' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.eco2Man_Tab())) {
            error = "Failed to click on 'ECO2Man' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'ECO2Man' tab.");

        //Add Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.eco2Man_Add_Button())) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.eco2Man_Add_Button())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        
        
        //Active / inactive
        
            
         if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.activeInactive_Dropdown())) {
            error = "Failed to wait for 'Active / inactive' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.activeInactive_Dropdown())) {
            error = "Failed to click on 'Active / inactive' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.text_Search(getData("Active / inactive")))) {
            error = "Failed to wait for Active / inactive: " + getData("Active / inactive");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Text2(getData("Active / inactive"))))
        {
            error = "Failed to wait for Active / inactive drop down option : " + getData("Active / inactive");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Text2(getData("Active / inactive"))))
        {
            error = "Failed to click Active / inactive down option : " + getData("Active / inactive");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Function: " + getData("Active / inactive"));
        
        
        
        
        //Actual / forecast
        
       if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.actualForcast_Dropdown())) {
            error = "Failed to wait for 'Actual / forecast' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.actualForcast_Dropdown())) {
            error = "Failed to click on 'Actual / forecast' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.text_Search(getData("Actual / forecast")))) {
            error = "Failed to wait for Actual / forecast: " + getData("Actual / forecast");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Text2(getData("Actual / forecast"))))
        {
            error = "Failed to wait for Actual / forecast drop down option : " + getData("Actual / forecast");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Text2(getData("Actual / forecast"))))
        {
            error = "Failed to click Actual / forecast down option : " + getData("Actual / forecast");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Function: " + getData("Actual / forecast"));
        
        
        
        //Year
        
            
         if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.ecoYear_Dropdown())) {
            error = "Failed to wait for 'Year' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.ecoYear_Dropdown())) {
            error = "Failed to click on 'Year' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.text_Search(getData("Year")))) {
            error = "Failed to wait for Year: " + getData("Year");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Text2(getData("Year"))))
        {
            error = "Failed to wait for Year drop down option : " + getData("Year");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Text2(getData("Year"))))
        {
            error = "Failed to click Yeardrop down option : " + getData("Year");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Function: " + getData("Year"));
        

        
        //Month
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.ecoMonth_Dropdown())) {
            error = "Failed to wait for 'Month' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.ecoMonth_Dropdown())) {
            error = "Failed to click on 'Month' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.text_Search(getData("Month")))) {
            error = "Failed to wait for Month: " + getData("Month");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Text2(getData("Month"))))
        {
            error = "Failed to wait for Month drop down option : " + getData("Month");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Text2(getData("Month"))))
        {
            error = "Failed to click Monthdrop down option : " + getData("Month");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Function: " + getData("Month"));
     
        
        
        
        //Quarter
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.ecoQuater_Dropdown())) {
            error = "Failed to wait for 'Quater' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.ecoQuater_Dropdown())) {
            error = "Failed to click on 'Quater' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.text_Search(getData("Quarter")))) {
            error = "Failed to wait for Quater: " + getData("Quater");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Text2(getData("Quarter"))))
        {
            error = "Failed to wait for Quater drop down option : " + getData("Quarter");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Text2(getData("Quarter"))))
        {
            error = "Failed to click Quaterdrop down option : " + getData("Quarter");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Function: " + getData("Quarter"));
        pause(3000);
        //Save Button
            if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.saveBtn())) {
                error = "Failed to wait for 'Save' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.saveBtn())) {
                error = "Failed to click on 'Save' button.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked the save button.");

            pause(5000);
            //Saving mask
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Project_Management_PageObject.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }

            //Save mask
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Project_Management_PageObject.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        
        //Approve / park project (entity level)
        
         if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.approvePark_Dropdown())) {
            error = "Failed to wait for 'Approve / park project (entity level)' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.approvePark_Dropdown())) {
            error = "Failed to click on 'Approve / park project (entity level)' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.text_Search(getData("Approve / park project (entity level)")))) {
            error = "Failed to wait for Month: " + getData("Approve / park project (entity level)");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Text2(getData("Approve / park project (entity level)"))))
        {
            error = "Failed to wait for Approve / park project (entity level) drop down option : " + getData("Month");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Text2(getData("Approve / park project (entity level)"))))
        {
            error = "Failed to click Approve / park project (entity level) drop down option : " + getData("Approve / park project (entity level)");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Function: " + getData("Approve / park project (entity level)"));

    //Save Button
            if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.saveBtn())) {
                error = "Failed to wait for 'Save' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.saveBtn())) {
                error = "Failed to click on 'Save' button.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked the save button.");

            pause(5000);
            //Saving mask
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Project_Management_PageObject.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }

            //Save mask
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Project_Management_PageObject.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
            
        return true;
    }

}
