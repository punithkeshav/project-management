/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Project_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Project_Management_V5_2_PageObject.Project_Management_PageObject;

/**
 *
 * @author LDisemelo
 */

@KeywordAnnotation(
        Keyword = "Delete Project Management",
        createNewBrowserInstance = false
)
public class FR10_Delete_Project_Management_MainScenario extends BaseClass {
    String error = "";
    
    public FR10_Delete_Project_Management_MainScenario()
    {
    
    }
    
     public TestResult executeTest() {
        if (!Navigate_To_Project_Management()) {
            return narrator.testFailed("Failed due - " + error);
        }
        if (!Delete_Project_Management()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully Deleted Record");
       }
     
      public boolean Navigate_To_Project_Management() {
        
        //Project Management
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.project_Management())) {
            error = "Failed to wait for Project Managenment Tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.project_Management())) {
            error = "Failed to click Projects Managenment Tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on Projects Managenment Tab");

   
        
        return true;
    }
      
    public boolean Delete_Project_Management() {
        
         pause(5000);
       if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.searchFilter())) {
            error = "Failed to wait for search button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.searchFilter())) {
            error = "Failed to click search button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on search button");
        
        
       pause(7200);
       
       //Select A record
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.selectRecord())) {
            error = "Failed to wait to Select A record.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.selectRecord())) {
            error = "Failed to click Select A record.";
            return false;
        }
        
        pause(7200);
        
        //Delete Record
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.deleteRecord())) {
            error = "Failed to wait for Delete Record.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.deleteRecord())) {
            error = "Failed to click Delete Record.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on Delete Record");
       pause(3200);
       
       SeleniumDriverInstance.switchToTabOrWindow();
   
         //Are you sure you want to delete this record?
         
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.yesButton())) {
            error = "Failed to wait for yes Button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.yesButton())) {
            error = "Failed to click yes Button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on yes Button");
        
        pause(5200);
        
        
        //Record Deleted
        SeleniumDriverInstance.switchToTabOrWindow();
        
           if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.okButton())) {
            error = "Failed to wait for yes Button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.okButton())) {
            error = "Failed to click yes Button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on yes Button");
        
        pause(5200);
        return true;
    }
      
      
      
      
        
}
