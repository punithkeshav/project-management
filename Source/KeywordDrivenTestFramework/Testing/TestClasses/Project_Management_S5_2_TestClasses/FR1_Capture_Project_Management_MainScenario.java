/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Project_Management_S5_2_TestClasses;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.Project_Management_V5_2_PageObject.Project_Management_PageObject;
import KeywordDrivenTestFramework.Entities.TestResult;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author LDisemelo
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Project Management v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_Project_Management_MainScenario extends BaseClass {
    String error = "";

    public FR1_Capture_Project_Management_MainScenario() {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

       public TestResult executeTest() {
        if (!Navigate_To_Project_Management()) {
            return narrator.testFailed("Failed due - " + error);
        }
        if (!Capture_Project_Management()) {
            return narrator.testFailed("Failed due - " + error);
        }
        
    
   //     if (testData.getData("Supporting Documents").equalsIgnoreCase("Yes")) {
      //      if (!Supporting_Documents()) {
             //   return narrator.testFailed("Failed due - " + error);
        //    }
        //} 
        return narrator.finalizeTest("Project Management record is saved");
   }

    public boolean Navigate_To_Project_Management() {
        
        //Project Management
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.project_Management())) {
            error = "Failed to wait for Project Managenment Tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.project_Management())) {
            error = "Failed to click Projects Managenment Tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on Projects Managenment Tab");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Add_Button(),8000)) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Add_Button())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");
        
        return true;
    }

    public boolean Capture_Project_Management() {
        //Process flow
        pause(3000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.ProcessFlow_Button())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.ProcessFlow_Button())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");

        //Entity dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.businessUnit_dropdown())) {
            error = "Failed to wait for 'Entity' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.businessUnit_dropdown())) {
            error = "Failed to click on 'Entity' dropdown.";
            return false;
        }
        
     //Entity TickBox
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.select_Entity())) {
            error = "Failed to wait for Entity : " + getData("Entity");
            return false;}
        
            if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.select_Entity())) {
            error = "Failed to wait for Entity : " + getData("Entity");
            return false;}
            
            pause(3500);
            if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Entitydrop_click())) {
            error = "Failed to wait for Entity : " + getData("Entity");
            return false;}
        
             narrator.stepPassedWithScreenShot("Entity: " + getData("Entity"));
             
     //Project
        if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.Project_Input(), getData("Project"))){
            error = "Failed to enter the Project field: " + getData("Project");
            return false;
        }
        narrator.stepPassedWithScreenShot("Project: " + getData("Project"));
        
     //Project description
     
     if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.Project_Description(), getData("Project description"))){
            error = "Failed to enter the Project description field: " + getData("Project description");
            return false;
        }
        narrator.stepPassedWithScreenShot("Project description: " + getData("Project description"));
       
      //Theme DropDown
       if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.theme_Dropdown())){
            error = "Failed to wait for the 'Theme' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.theme_Dropdown())){
            error = "Failed to click the 'Theme' dropdown.";
            return false;
        }
        
      if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.theme_Select())){
            error = "Failed to wait for the 'Theme Select'.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.theme_Select())){
            error = "Failed to click the 'Theme Select'.";
            return false;
        }
        
        pause(3500);
        
       if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.themedrop_click())) {
            error = "Failed to wait for Theme : " + getData("Theme");
            return false;}
       
     narrator.stepPassedWithScreenShot("Theme Option Selected from Dropdown Options");

        pause(4000);

        //Function
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Function_dropdown())) {
            error = "Failed to wait for 'Function' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Function_dropdown())) {
            error = "Failed to click on 'Function' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.text_Search(getData("Function")))) {
            error = "Failed to wait for Function: " + getData("Function");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Text2(getData("Function"))))
        {
            error = "Failed to wait for Function drop down option : " + getData("Function");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Text2(getData("Function"))))
        {
            error = "Failed to click Function drop down option : " + getData("Function");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Function: " + getData("Function"));
        
        //Project Category
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.projectCategoryDD())) {
            error = "Failed to wait for 'Project category' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.projectCategoryDD())) {
            error = "Failed to click on 'Project category' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.text_Search(getData("Project category")))) {
            error = "Failed to wait for Project category: " + getData("Project category");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Text2(getData("Project category"))))
        {
            error = "Failed to wait for Project category drop down option : " + getData("Project category");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Text2(getData("Project category"))))
        {
            error = "Failed to click Project category drop down option : " + getData("Project category");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Project category: " + getData("Project category"));
        
        //Project type
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.projectTypeDD())) {
            error = "Failed to wait for 'Project type' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.projectTypeDD())) {
            error = "Failed to click on 'Project type' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.text_Search(getData("Project type")))) {
            error = "Failed to wait for Project type: " + getData("Project type");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Text2(getData("Project type"))))
        {
            error = "Failed to wait for Project type drop down option : " + getData("Project type");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Text2(getData("Project type"))))
        {
            error = "Failed to click Project type drop down option : " + getData("Project type");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Project type: " + getData("Project type"));

        //Planned start date
        if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.PlannedStartDate(), startDate + 10000)){
            error = "Failed to enter the planned start date";
            return false;
        }
        narrator.stepPassedWithScreenShot("Planned start date: " + startDate);
        
        //Planned completion date
        if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.PlannedCompletionDate(), endDate + 100000)){
            error = "Failed to enter the planned completion date";
            return false;
        }
        narrator.stepPassedWithScreenShot("Planned completion date: " + endDate);
        
        //Objectives and proposed activities
        if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.ObjectivesAndProposedActivities(), getData("Objectives and proposed activities"))){
            error = "Failed to enter the Objectives and proposed activities";
            return false;
        }
        narrator.stepPassedWithScreenShot("Objectives and proposed activities: " + getData("Objectives and proposed activities"));
        
        //Due diligence notes
        if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.DueDiligenceNotes(), getData("Due diligence notes"))){
            error = "Failed to enter the Due diligence notes";
            return false;
        }
        narrator.stepPassedWithScreenShot("Due diligence end date: " + getData("Due diligence notes"));
        SeleniumDriverInstance.scrollDown();
        pause(5000);
        
     SeleniumDriverInstance.scrollToElement(Project_Management_PageObject.ProjectOriginator_dropdown());

         pause(5000);
        //Project originator
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.ProjectOriginator_dropdown())) {
            error = "Failed to wait for 'Project originator' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.ProjectOriginator_dropdown())) {
            error = "Failed to click on 'Project originator' dropdown.";
            return false;
        }

       if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.text_Search2())) {
            error = "Failed to wait for Project originator: " + getData("Project originator");
            return false;
        }

             if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.text_Search2())) {
            error = "Failed to wait for Project originator: " + getData("Project originator");
            return false;
        }

        pause(3000);

        if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.text_Search2(),(getData("Project originator1")))) {
            error = "Failed to click the Project originator: " + getData("Project originator");
            return false;
        }
        pause(3000);
        if (!SeleniumDriverInstance.pressEnter())
        {
            error = "Failed to press enter";
            return false;
        }
         pause(2000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Text2(getData("Project originator"))))
        {
            error = "Failed to wait for Project originator option : " + getData("Project originator");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Text2(getData("Project originator"))))
        {
            error = "Failed to click Project originator option : " + getData("Project originator");
            return false;
        }

        narrator.stepPassedWithScreenShot("Project originator: " + getData("Project originator"));
         pause(3000);
        SeleniumDriverInstance.scrollDown();
        pause(3000);

      //Technicallity Feasisbitly
      
       if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.technicalFeasibilityDD())) {
            error = "Failed to wait for 'Technical feasibility' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.technicalFeasibilityDD())) {
            error = "Failed to click on 'Technical feasibility' dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.text_Search3(),(getData("Technical feasibility")))) {
            error = "Failed to click the Technical feasibility: " + getData("Technical feasibility");
            return false;
        }
        pause(5000);
        if (!SeleniumDriverInstance.pressEnter())
        {
            error = "Failed to press enter";
            return false;
        }
         pause(5000);
       //
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Text2(getData("Technical feasibility"))))
        {
            error = "Failed to wait for Technical feasibility drop down option : " + getData("Technical feasibility");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Text2(getData("Technical feasibility"))))
        {
            error = "Failed to click Project type drop down option : " + getData("Technical feasibility");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Technical feasibility: " + getData("Technical feasibility"));
        
        //comments
        
     if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.technicalFeasibilityComments(), getData("Comments"))){
            error = "Failed to enter the Comments: " + getData("Comments");
            return false;
        }
        narrator.stepPassedWithScreenShot("Comments: " + getData("Comments"));
        
      
      //Do you have budget?
 if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.budgetDD())) {
            error = "Failed to wait for 'Do you have budget?' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.budgetDD())) {
            error = "Failed to click on 'Do you have budget?' dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.text_Search3(),(getData("Do you have budget?")))) {
            error = "Failed to click the Do you have budget?: " + getData("Do you have budget?");
            return false;
        }
        pause(5000);
        if (!SeleniumDriverInstance.pressEnter())
        {
            error = "Failed to press enter";
            return false;
        }
         pause(5000);
       //
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Text2(getData("Do you have budget?"))))
        {
            error = "Failed to wait for Do you have budget? drop down option : " + getData("Do you have budget?");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Text2(getData("Do you have budget?"))))
        {
            error = "Failed to click Project type drop down option : " + getData("Do you have budget?");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Do you have budget?: " + getData("Do you have budget?"));
	
	
	
	   if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.budgetComments(), getData("Budget Comment"))){
            error = "Failed to enter the Budget Comment: " + getData("Budget Comment");
            return false;
        }
        narrator.stepPassedWithScreenShot("Budget Comment: " + getData("Budget Comment"));

     //Human Resource
        
         if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.humanResourcesDD())) {
            error = "Failed to wait for 'Do you have human resources?' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.humanResourcesDD())) {
            error = "Failed to click on 'Do you have human resources?' dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.text_Search3(),(getData("Do you have human resources?")))) {
            error = "Failed to click the Do you have human resources?: " + getData("Do you have human resources?");
            return false;
        }
        pause(5000);
        if (!SeleniumDriverInstance.pressEnter())
        {
            error = "Failed to press enter";
            return false;
        }
         pause(5000);
       //
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Text2(getData("Do you have human resources?"))))
        {
            error = "Failed to wait for Do you have human resources? drop down option : " + getData("Do you have human resources?");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Text2(getData("Do you have human resources?"))))
        {
            error = "Failed to click Project type drop down option : " + getData("Do you have human resources?");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Do you have human resources?: " + getData("Do you have human resources?"));
	
	
	
	   if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.humanResourceComment(), getData("Comment"))){
            error = "Failed to enter the Comment: " + getData("Comment");
            return false;
        }
        narrator.stepPassedWithScreenShot("Comment: " + getData("Comment"));

            // Team Name
    
//      if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.teamNameDD()))
//        {
//            error = "Failed to wait for 'Team Name' dropdown..";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.teamNameDD()))
//        {
//            error = "Failed to click Team Name dropdown.";
//            return false;
//        }
        
//         pause(3000);
//        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Text2(getData("Team Name"))))
//        {
//            error = "Failed to wait for Team Name drop down option : " + getData("Team Name");
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Text2(getData("Team Name"))))
//        {
//            error = "Failed to click Team Name drop down option : " + getData("Team Name");
//            return false;
//        }
//
//        narrator.stepPassedWithScreenShot("Team Name: '" + getData("Team Name") + "'.");
        
       //
        pause(6000);
        if(getData("Save to continue").equalsIgnoreCase("Yes")){
            //Save Button
            if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.SaveToContinue_saveBtn())) {
                error = "Failed to wait for 'Save to continue' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.SaveToContinue_saveBtn())) {
                error = "Failed to click on 'Save to continue' button.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked the Save to continue button.");
        } else {
            //Save Button
            if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.saveBtn())) {
                error = "Failed to wait for 'Save' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.saveBtn())) {
                error = "Failed to click on 'Save' button.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked the save button.");
        }
        pause(5000);
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Project_Management_PageObject.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Project_Management_PageObject.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Project_Management_PageObject.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Project_Management_PageObject.validateSave());

        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");
        
        SeleniumDriverInstance.scrollToElement(Project_Management_PageObject.StrategicObjectives_tab());
        pause(5000);
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");

        return true;
    }
    

}
