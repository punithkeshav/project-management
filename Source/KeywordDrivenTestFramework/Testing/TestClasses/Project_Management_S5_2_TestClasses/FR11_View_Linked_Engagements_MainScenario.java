/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Project_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Project_Management_V5_2_PageObject.Project_Management_PageObject;

/**
 *
 * @author LDisemelo
 */

@KeywordAnnotation(
        Keyword = "View Linked Engagements",
        createNewBrowserInstance = false
)
public class FR11_View_Linked_Engagements_MainScenario extends BaseClass {
    String error = "";
    
    public FR11_View_Linked_Engagements_MainScenario()
    {
    
    
    }
    
    
     public TestResult executeTest() {
         
     if (!Navigate_To_Project_Management()) {
            return narrator.testFailed("Failed due - " + error);
        }

      if (!View_Linked_Engagements()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("View Linked Engagements");
       }
     
     
        public boolean Navigate_To_Project_Management() {
        
        //Project Management
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.project_Management())) {
            error = "Failed to wait for Project Managenment Tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.project_Management())) {
            error = "Failed to click Projects Managenment Tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on Projects Managenment Tab");
     pause(5000);
       if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.searchFilter())) {
            error = "Failed to wait for search button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.searchFilter())) {
            error = "Failed to click search button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on search button");
        
        
       pause(7200);
       
       //Select A record
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.selectRecord())) {
            error = "Failed to wait to Select A record.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.selectRecord())) {
            error = "Failed to click Select A record.";
            return false;
        }
         narrator.stepPassedWithScreenShot("Successfully clicked on Select A record");
        pause(7200);
            
        // Navigate to Project actions
       if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.ProjectActions_tab())) {
            error = "Failed to wait for Project Actions Tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.ProjectActions_tab())) {
            error = "Failed to click Projects Actions Tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on Projects Actions Tab");
       
       pause(3500);
   
        
        return true;
    }

       
       public boolean View_Linked_Engagements() {
        
        //Navigate to Linked Engagements
        pause(3500);
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.LinkedEngagements_tab())) {
            error = "Failed to wait for Linked Engagements Tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.LinkedEngagements_tab())) {
            error = "Failed to click Linked Engagements Tab.";
            return false;
        }
  
        narrator.stepPassedWithScreenShot("Successfully clicked on Navigate to Linked Engagements");
      pause(5500);
      
      //No linked records to click
       if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.LinkedEngagementsSearch())) {
            error = "Failed to wait for search butto.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.LinkedEngagementsSearch())) {
            error = "Failed to click search butto.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked on search button");
        
        
         if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.LinkedEngagementsSearch1())) {
            error = "Failed to wait for search butto.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.LinkedEngagementsSearch1())) {
            error = "Failed to click search butto.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked on search button");
        pause(5500);
     return true;
           }
     
    }
    
    

