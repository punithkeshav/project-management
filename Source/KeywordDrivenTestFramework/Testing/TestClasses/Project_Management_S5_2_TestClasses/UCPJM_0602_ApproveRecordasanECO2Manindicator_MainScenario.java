/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Project_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Project_Management_V5_2_PageObject.Project_Management_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;

/**
 *
 * @author LDisemelo
 */

@KeywordAnnotation(
        Keyword = "Approve Record as an ECO2Man indicator",
        createNewBrowserInstance = false
)
public class UCPJM_0602_ApproveRecordasanECO2Manindicator_MainScenario extends BaseClass {
    
     String error = "";
     SikuliDriverUtility sikuliDriverUtility;
     
     public UCPJM_0602_ApproveRecordasanECO2Manindicator_MainScenario(){
          this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
     }
    
        public TestResult executeTest() {
        if (!ApproveRecordAsAnECO2ManIndicator()) {
         return narrator.testFailed("Failed due - " + error);
        }
     
        return narrator.finalizeTest("Capture Energy and GHG Emissions Savings ");
    }
     
        
        public boolean ApproveRecordAsAnECO2ManIndicator(){
            
        //Approved as an ECO2man indicator
        pause(5000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.approvedEco2ManIndicatorDD())){
            error = "Failed to wait for the Approved as an ECO2man indicator Field.";
            return false;
        }
       
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.approvedEco2ManIndicatorDD())){
            error = "Failed to click the Approved as an ECO2man indicator Field.";
            return false;
        }
        
         if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Text2(getData("Approved as an ECO2man indicator")))){
            error = "Failed to enter the Approved as an ECO2man indicator Field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully completed the Approved as an ECO2man indicator Field.");
        
        
        pause(3000);
        
        //Comments
        
         if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.comments2())){
            error = "Failed to click the Comments Field.";
            return false;
        }
        
         if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.comments2(),getData("Comments"))){
            error = "Failed to enter the Target Field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully completed the Comments Field.");
        
        //Save
        //Save Button
            if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.saveBtn1())) {
                error = "Failed to wait for 'Save' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.saveBtn1())) {
                error = "Failed to click on 'Save' button.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked the save button.");
        pause(5000);
        
         return true;
        }
}

