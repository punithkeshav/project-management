/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Project_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Project_Management_V5_2_PageObject.Project_Management_PageObject;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author LDisemelo
 */

@KeywordAnnotation(
        Keyword = "UCPJM 0202_Reviewing_Registered_Project_MainScenario",
        createNewBrowserInstance = false
)

public class UCPJM_0202_Reviewing_Registered_Project_MainScenario extends BaseClass {
    String error = "";

    public UCPJM_0202_Reviewing_Registered_Project_MainScenario() {
     
    }

    public TestResult executeTest() {
        if (!BusinessUnitVerification()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("	Project Management record is saved");
    }

    public boolean BusinessUnitVerification () {
        
        pause(3000);
        
       SeleniumDriverInstance.scrollToElement(Project_Management_PageObject.projectApprovalTab());
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.projectApprovalTab())){
            error = "Failed to wait for the Project Approval Tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.projectApprovalTab())){
            error = "Failed to click the Project Approval Tab.";
            return false;
        }
        
        pause(2500);
        //SeleniumDriverInstance.scrollToElement(Project_Management_PageObject.businesssUnit_Checkbox());
                
        //Tick Business unit verification and approval user Checkbox
        if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.businesssUnit_Checkbox())){
            error = "Failed to wait for the Tick Business unit verification and approval user Checkbox.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.businesssUnit_Checkbox())){
            error = "Failed to click the Tick Business unit verification and approval user Checkbox.";
            return false;
        }
        narrator.finalizeTest("Successfully ticked the Tick Business unit verification and approval user Checkbox");
        
        

         pause(5000);
  
//           if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.groupApporoval_Checkbox())){
//            error = "Failed to click the Tick the “Group Approval” checkbox.";
//            return false;
//           }
        
            
           //Group Approval DropDown
           
         if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.groupVerification_dropdown())){
            error = "Failed to wait for the Group Approval DropDown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.groupVerification_dropdown())){
            error = "Failed to click the Group Approval DropDown.";
            return false;
        }
        
        pause(2500);
       if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.text_Search2()))
            {
                error = "Failed to wait for Group verification and approval.";
                return false;
            }
       
        if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.text_Search2(), getData("Group verification and approval")))
            {
                 error = "Failed to enter Group verification and approval :" + getData("Group verification and approval");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }
          
       if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Text2(getData("Group verification and approval")))){
            error = "Failed to wait for the Group Approval DropDown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Text2(getData("Group verification and approval")))){
            error = "Failed to click the Group Approval DropDown.";
            return false;
        }
        
             narrator.finalizeTest("Successfully selectedGroup Approval DropDown");
             
         pause(3500);
         
         //Tick the “Group Approval” checkbox
         
        if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.groupApporoval_Checkbox())){
            error = "Failed to wait for the Tick the “Group Approval” checkbox.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.groupApporoval_Checkbox())){
            error = "Failed to click the Tick the “Group Approval” checkbox.";
            return false;
        }
        
       narrator.finalizeTest("Successfully ticked the Tick the “Group Approval” checkbox");
        
        
        
   
       
        
        //Scroll to element
        SeleniumDriverInstance.scrollToElement(Project_Management_PageObject.StrategicObjectives_tab());
        narrator.finalizeTest("The Project Approval, Project Financials and ECO2Man tabs are displayed.");
         pause(5000);
        //Save Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.saveBtn())) {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.saveBtn())) {
            error = "Failed to click on 'Save' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the save button.");

        pause(6000);


        return true;
    }
   

}
