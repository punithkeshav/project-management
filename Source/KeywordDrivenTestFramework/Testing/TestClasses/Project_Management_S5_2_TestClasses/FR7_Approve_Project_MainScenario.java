/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Project_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.Project_Management_V5_2_PageObject.Project_Management_PageObject;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;

/**
 *
 * @author LDisemelo
 */

@KeywordAnnotation(
        Keyword = "FR7-Approve Project Actual",
        createNewBrowserInstance = false
)
public class FR7_Approve_Project_MainScenario extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR7_Approve_Project_MainScenario(){
    this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    
    
    }
    
    
    public TestResult executeTest() {
        if (!AprroveProject()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully Captured Project Management / Forecasted Budget record is saved.");
    }
    
    public boolean AprroveProject(){
    
          pause(5000);
        //Project Financials
        if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.ProjectFinancials_Tab())){
            error = "Failed to wait for the Project Financials tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.ProjectFinancials_Tab())){
            error = "Failed to click the Project Financials tab.";
            return false;
        }
        //Total budget field
        
   
        //Total Budget
        if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.TotalBudget())){
            error = "Failed to wait for the Total Budget Field.";
            return false;
        }
       
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.TotalBudget())){
            error = "Failed to click the Total Budget Field.";
            return false;
        }
        
         if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.TotalBudget(), getData("Total Budget"))){
            error = "Failed to enter the Total Budget Field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully completed the Total Budget Field.");
        
        //Budget extension tick box
       if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.budgetExtension_Checkbox())){
            error = "Failed to wait for the Tick Budget extension tick box Checkbox.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.budgetExtension_Checkbox())){
            error = "Failed to click the Tick Budget extension tick box Checkbox.";
            return false;
        }
        narrator.finalizeTest("Successfully ticked the Tick Budget extension tick box");
         pause(3000);

        //Budget extension Field
        if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.TotalExtension())){
            error = "Failed to wait for the Total extension Field.";
            return false;
        }
       
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.TotalExtension())){
            error = "Failed to click the Total extension Field.";
            return false;
        }
        
         if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.TotalExtension(), getData("Total extension"))){
            error = "Failed to enter the Total extension Field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully completed the Total extension Field.");
        
        
        //Total budget with extension
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.TotalBudgetWithExtension())){
            error = "Failed to wait for the Total budget with extension Field.";
            return false;
        }
       
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.TotalBudgetWithExtension())){
            error = "Failed to click the Total budget with extension Field.";
            return false;
        }
        
         if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.TotalBudgetWithExtension(), getData("Total budget with extension"))){
            error = "Failed to enter the Total budget with extension Field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully completed the Total budget with extension Field.");
        
        //Reporting Frequency Drop Down
        
       if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.reportingFrequencyDD())) {
            error = "Failed to wait for 'Function' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.reportingFrequencyDD())) {
            error = "Failed to click on 'Function' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.text_Search(getData("Reporting Frequency")))) {
            error = "Failed to wait for Reporting Frequency: " + getData("Reporting Frequency");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Text2(getData("Reporting Frequency"))))
        {
            error = "Failed to wait for Reporting Frequency drop down option : " + getData("Reporting Frequency");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Text2(getData("Reporting Frequency"))))
        {
            error = "Failed to click Reporting Frequency drop down option : " + getData("Reporting Frequency");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Function: " + getData("Reporting Frequency"));
        
        //
        pause(3000);
        //Navigate to the Total Costs panel and capture the details
                
       if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.totalCosts_Panel())) {
            error = "Failed to wait for 'Total Costs panel.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.totalCosts_Panel())) {
            error = "Failed to click on 'Total Costs panel.";
            return false;
        }
        
       if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.totalCosts_Panel())) {
            error = "Failed to click on 'Total Costs panel.";
            return false;
        }
         pause(3000);
         
        //Payback period in years
        
       if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.paybackPeriodInYears())){
            error = "Failed to wait for the Payback period in years Field.";
            return false;
        }
       
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.paybackPeriodInYears())){
            error = "Failed to click the Total budget with extension Field.";
            return false;
        }
        
         if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.paybackPeriodInYears(), getData("Payback period in years"))){
            error = "Failed to enter the Total budget with extension Field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully completed the Payback period in years Field.");
        
        //Simple payback
        
      if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.simplePayback())){
            error = "Failed to wait for the Simple payback.";
            return false;
        }
       
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.simplePayback())){
            error = "Failed to click the Total budget with Simple payback.";
            return false;
        }
        
         if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.simplePayback(), getData("Simple payback"))){
            error = "Failed to enter the Total budget with extension Field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully completed the Simple payback Field.");
        
        //Net present value
       if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.netPresentValue())){
            error = "Failed to wait for the Net present value.";
            return false;
        }
       
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.netPresentValue())){
            error = "Failed to click the Total budget with Net present value.";
            return false;
        }
        
         if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.netPresentValue(), getData("Net present value"))){
            error = "Failed to enter the TNet present value Field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully completed the Net present value Field.");
        
        //Cost before incentives
        
       if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.costBeforeIncentives())){
            error = "Failed to wait for the Cost before incentives value.";
            return false;
        }
       
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.costBeforeIncentives())){
            error = "Failed to click the Total budget with Cost before incentives value.";
            return false;
        }
        
         if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.costBeforeIncentives(), getData("Cost before incentives"))){
            error = "Failed to enter the Cost before incentives Field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully completed the Cost before incentives Field.");
        
        //Total cost
       if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.totalCost())){
            error = "Failed to wait for the Total cost value.";
            return false;
        }
       
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.totalCost())){
            error = "Failed to click the Total budget with Total cost value.";
            return false;
        }
        
         if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.totalCost(), getData("Total cost"))){
            error = "Failed to enter the Total cost Field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully completed the Total cost Field.");
        
        //ROI percentage
        if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.rOIPercentage())){
            error = "Failed to wait for the ROI percentage value.";
            return false;
        }
       
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.rOIPercentage())){
            error = "Failed to click the ROI percentage value.";
            return false;
        }
        
         if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.rOIPercentage(), getData("ROI percentage"))){
            error = "Failed to enter the ROI percentage Field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully completed the ROI percentage Field.");
        
        //Internal rate of return
        if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.internalRateOfReturn())){
            error = "Failed to wait for the Internal rate of return value.";
            return false;
        }
       
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.internalRateOfReturn())){
            error = "Failed to click the Internal rate of return value.";
            return false;
        }
        
         if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.internalRateOfReturn(), getData("Internal rate of return"))){
            error = "Failed to enter the Internal rate of return Field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully completed the Internal rate of return Field.");
        
        //Loan cost
     if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.loanCost())){
            error = "Failed to wait for the Loan cost value.";
            return false;
        }
       
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.loanCost())){
            error = "Failed to click the Loan cost return value.";
            return false;
        }
        
         if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.loanCost(), getData("Loan cost"))){
            error = "Failed to enter the Loan cost Field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully completed the Loan cost Field.");
        
        
        //Financial incentives
        
      if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.financialIncentives())){
            error = "Failed to wait for the Financial incentives value.";
            return false;
        }
       
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.financialIncentives())){
            error = "Failed to click the Financial incentives value.";
            return false;
        }
        
         if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.financialIncentives(), getData("Financial incentives"))){
            error = "Failed to enter the Financial incentives Field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully completed the Financial incentivest Field.");
        
        //Project ActualsCosts panel 
        
        SeleniumDriverInstance.scrollToElement(Project_Management_PageObject.ProjectActuals_Panel());
        
       if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.ProjectActuals_Panel())){
            error = "Failed to wait for the Project Running Costs panel.";
            return false;
        }
       
//        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.projectRunningCostsPanel())){
//            error = "Failed to click the Project Running Costs panel.";
//            return false;
//        }
//        
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.ProjectActuals_Panel())){
            error = "Failed to click the Project Running Costs panel.";
            return false;
        }

         narrator.stepPassedWithScreenShot("Project Running Costs panel.");
    pause(5000);    
    
     SeleniumDriverInstance.switchToFrameByXpath(Project_Management_PageObject.ProjectActuals_Panel());
        //Add Project Running Costs
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.ProjectActuals_Add_Button())) {
                error = "Failed to wait for 'Add' button.";
                return false;
            }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.ProjectActuals_Add_Button())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
  
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");
            
          //Year
        
         if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Year_Dropdown())) {
            error = "Failed to wait for 'Year' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Year_Dropdown())) {
            error = "Failed to click on 'Year' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.text_Search(getData("Year")))) {
            error = "Failed to wait for Year: " + getData("Year");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Text2(getData("Year"))))
        {
            error = "Failed to wait for Year drop down option : " + getData("Year");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Text2(getData("Year"))))
        {
            error = "Failed to click Yeardrop down option : " + getData("Year");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Function: " + getData("Year"));
          
  
          //Month
 if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Month_Dropdown())) {
            error = "Failed to wait for 'Month' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Month_Dropdown())) {
            error = "Failed to click on 'Month' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.text_Search(getData("Month")))) {
            error = "Failed to wait for Month: " + getData("Month");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Text2(getData("Month"))))
        {
            error = "Failed to wait for Month drop down option : " + getData("Month");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Text2(getData("Month"))))
        {
            error = "Failed to click Monthdrop down option : " + getData("Month");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Function: " + getData("Month"));
     
          
          //Quater
       if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Quater_Dropdown())) {
            error = "Failed to wait for 'Quater' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Quater_Dropdown())) {
            error = "Failed to click on 'Quater' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.text_Search(getData("Quarter")))) {
            error = "Failed to wait for Quater: " + getData("Quater");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Text2(getData("Quarter"))))
        {
            error = "Failed to wait for Quater drop down option : " + getData("Quarter");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Text2(getData("Quarter"))))
        {
            error = "Failed to click Quaterdrop down option : " + getData("Quarter");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Function: " + getData("Quarter"));

       //Budget for this period

       if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.budgetedForThisPeriod())){
            error = "Failed to wait for the budgetedForThisPeriod value.";
            return false;
        }
       
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.budgetedForThisPeriod())){
            error = "Failed to click the budgetedForThisPeriod value.";
            return false;
        }
        
         if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.budgetedForThisPeriod(), getData("budgetedForThisPeriod"))){
            error = "Failed to enter the budgetedForThisPeriod Field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully completed the budgeted For This PeriodField.");

          //Expenditure
          if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.expenditure())){
            error = "Failed to wait for the Expenditure value.";
            return false;
        }
       
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.expenditure())){
            error = "Failed to click the actual Expenditure value.";
            return false;
        }
        
         if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.expenditure(), getData("Expenditure"))){
            error = "Failed to enter the Expenditure Field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully completed the Expenditure Field.");

          //Comments
          
        if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.comments1())){
            error = "Failed to wait for the comments value.";
            return false;
        }
       
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.comments1())){
            error = "Failed to click the comments return value.";
            return false;
        }
        
         if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.comments1(), getData("Comments"))){
            error = "Failed to enter the comments Field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully completed the comments Field.");
        //
        
        
    //Save Button
            if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.saveBtn())) {
                error = "Failed to wait for 'Save' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.saveBtn())) {
                error = "Failed to click on 'Save' button.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked the save button.");

            pause(5000);
            //Saving mask
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Project_Management_PageObject.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }

            //Save mask
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Project_Management_PageObject.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        
        //Approve / park project (entity level)
        
         if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.approvePark_Dropdown())) {
            error = "Failed to wait for 'Approve / park project (entity level)' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.approvePark_Dropdown())) {
            error = "Failed to click on 'Approve / park project (entity level)' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.text_Search(getData("Approve / park project (entity level)")))) {
            error = "Failed to wait for Month: " + getData("Approve / park project (entity level)");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Text2(getData("Approve / park project (entity level)"))))
        {
            error = "Failed to wait for Approve / park project (entity level) drop down option : " + getData("Month");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Text2(getData("Approve / park project (entity level)"))))
        {
            error = "Failed to click Approve / park project (entity level) drop down option : " + getData("Approve / park project (entity level)");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Function: " + getData("Approve / park project (entity level)"));

    //Save Button
            if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.saveBtn())) {
                error = "Failed to wait for 'Save' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.saveBtn())) {
                error = "Failed to click on 'Save' button.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked the save button.");

            pause(5000);
            //Saving mask
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Project_Management_PageObject.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }

            //Save mask
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Project_Management_PageObject.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
        
            }
        
        
    return true;
    }


}