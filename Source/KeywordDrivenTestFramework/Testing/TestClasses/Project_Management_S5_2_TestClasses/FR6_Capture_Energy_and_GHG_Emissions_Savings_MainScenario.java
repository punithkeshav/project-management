/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Project_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Project_Management_V5_2_PageObject.Project_Management_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;

/**
 *
 * @author LDisemelo
 */

@KeywordAnnotation(
        Keyword = "Capture Energy and GHG Emissions Savings",
        createNewBrowserInstance = false
)
public class FR6_Capture_Energy_and_GHG_Emissions_Savings_MainScenario extends BaseClass {
    
     String error = "";
     SikuliDriverUtility sikuliDriverUtility;
    
    
    public FR6_Capture_Energy_and_GHG_Emissions_Savings_MainScenario(){
         this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        
    }
    
    public TestResult executeTest() {
        if (!CaptureEnergyGHGEmissionsSavings()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Capture Energy and GHG Emissions Savings ");
    }
        public boolean CaptureEnergyGHGEmissionsSavings(){
            
         //Project complete (group level)
        
         if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.projectComplete_Dropdown())) {
            error = "Failed to wait for 'Project complete (group level)(entity level)' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.projectComplete_Dropdown())) {
            error = "Failed to click on 'Project complete (group level) (entity level)' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.text_Search(getData("Project complete (group level)")))) {
            error = "Failed to wait for Month: " + getData("Project complete (group level)");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Text2(getData("Project complete (group level)"))))
        {
            error = "Failed to wait for Project complete (group level) (entity level) drop down option : " + getData("Project complete (group level)");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Text2(getData("Project complete (group level)"))))
        {
            error = "Failed to click Project complete (group level) (entity level) drop down option : " + getData("Project complete (group level)");
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Function: " + getData("Project complete (group level)"));   
            
            
         SeleniumDriverInstance.scrollToElement(Project_Management_PageObject.eco2Man_Tab());
         
        //Navigate to ECO2Man Tab
        pause(2000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.eco2Man_Tab())) {
            error = "Failed to wait for 'ECO2Man' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.eco2Man_Tab())) {
            error = "Failed to click on 'ECO2Man' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'ECO2Man' tab.");
        
        //Edit Eco2man
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.editEco2man())) {
            error = "Failed to wait for 'ECO2Man' edit.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.editEco2man())) {
            error = "Failed to click on 'ECO2Man' edit.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'ECO2Man' edit.");
        pause(12000);
        
        
        //Add
          //Add Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.energySavings_Add_Button())) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.energySavings_Add_Button())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");
        pause(3000);
        //Emission source
    if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.emmisionSource_Dropdown())) {
            error = "Failed to wait for 'Emission source' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.emmisionSource_Dropdown())) {
            error = "Failed to click on 'Emission source' dropdown.";
            return false;
        }
        pause(5000);

         if (!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.text_Search3(),(getData("Emission source")))) {
            error = "Failed to click the Emission source: " + getData("Emission source");
            return false;
        }
        
        if (!SeleniumDriverInstance.pressEnter())
        {
            error = "Failed to press enter";
            return false;
        }
 pause(3000);
//     
//        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.text_Search(getData("Emission source1")))) {
//            error = "Failed to wait Emission source: " + getData("Emission source");
//            return false;
//        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.Text2(getData("Emission source1"))))
        {
            error = "Failed to wait for Emission source drop down option : " + getData("Emission source");
            return false;
        }
        if (!SeleniumDriverInstance.doubleClickElementbyXpath(Project_Management_PageObject.Text2(getData("Emission source1"))))
        {
            error = "Failed to click Emission source drop down option : " + getData("Emission source");
            return false;
        }
           if (!SeleniumDriverInstance.doubleClickElementbyXpath(Project_Management_PageObject.Text2(getData("Emission source2"))))
        {
            error = "Failed to click Emission source drop down option : " + getData("Emission source1");
            return false;
        }
          if (!SeleniumDriverInstance.doubleClickElementbyXpath(Project_Management_PageObject.Text2(getData("Emission source3"))))
        {
            error = "Failed to click Emission source drop down option : " + getData("Emission source2");
            return false;
        }
          
         if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Text2(getData("Emission source4"))))
        {
            error = "Failed to click Emission source drop down option : " + getData("Emission source4");
            return false;
        }
        narrator.stepPassedWithScreenShot("Function: " + getData("Emission source4")); 
        
        
        //Measurement 
        
       if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.measurement())){
            error = "Failed to wait for the Measurement Field.";
            return false;
        }
       
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.measurement())){
            error = "Failed to click the Measurement Field.";
            return false;
        }
        
         if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.measurement(), getData("Measurement"))){
            error = "Failed to enter the Measurement Field.";
            return false;
        }

//        if (!SeleniumDriverInstance.pressEnter())
//        {
//            error = "Failed to press enter";
//            return false;
//        }
        narrator.stepPassedWithScreenShot("Successfully completed the Measurement Field.");
           
        pause(3000);
        //Unit DropDown
            
       if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.unit_Dropdown())){
            error = "Failed to wait for the Unit Field.";
            return false;
        }
       
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.unit_Dropdown())){
            error = "Failed to click the Unit Field.";
            return false;
        }
        
         if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Text2(getData("Unit")))){
            error = "Failed to enter the Unit Field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully completed the Unit Field.");
        
        
        //Target
         if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.target())){
            error = "Failed to wait for the Target Field.";
            return false;
        }
       
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.target())){
            error = "Failed to click the Target Field.";
            return false;
        }
        
         if(!SeleniumDriverInstance.enterTextByXpath(Project_Management_PageObject.target(),getData("Target"))){
            error = "Failed to enter the Target Field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully completed the Target Field.");
        
        //
        
           if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.unit1_Dropdown())){
            error = "Failed to wait for the Unit Field.";
            return false;
        }
       
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.unit1_Dropdown())){
            error = "Failed to click the Unit Field.";
            return false;
        }
        
         if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.Text2(getData("Unit1")))){
            error = "Failed to enter the Unit Field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully completed the Unit Field.");
        
        pause(2500);
        //Save Button
            if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.saveBtn1())) {
                error = "Failed to wait for 'Save' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.saveBtn1())) {
                error = "Failed to click on 'Save' button.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked the save button.");

            pause(8000);
            //Saving mask
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Project_Management_PageObject.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }

        
        
        
        
        
        
        
        
            return true;
        }
}
