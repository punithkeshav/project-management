/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Project_Management_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Project_Management_V5_2_PageObject.Project_Management_PageObject;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author LDisemelo
 */

@KeywordAnnotation(
        Keyword = "UCPJM 0202_Reviewing_Registered_Project_AlternateScenario",
        createNewBrowserInstance = false
)

public class UCPJM_0202_Reviewing_Registered_Project_AlternateScenario extends BaseClass {
    String error = "";

    public UCPJM_0202_Reviewing_Registered_Project_AlternateScenario() {
     
    }

    public TestResult executeTest() {
        if (!BusinessUnitVerification()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("	Project Management record is saved");
    }

    public boolean BusinessUnitVerification () {
        
        pause(3500);
        
        
        
        SeleniumDriverInstance.scrollToElement(Project_Management_PageObject.projectApprovalTab());
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.projectApprovalTab())){
            error = "Failed to wait for the Project Approval Tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.projectApprovalTab())){
            error = "Failed to click the Project Approval Tab.";
            return false;
        }
                
        //Tick Business unit verification and approval user Checkbox
        
        SeleniumDriverInstance.scrollToElement(Project_Management_PageObject.businesssUnit_Checkbox());
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.businesssUnit_Checkbox())){
            error = "Failed to wait for the Tick Business unit verification and approval user Checkbox.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.businesssUnit_Checkbox())){
            error = "Failed to click the Tick Business unit verification and approval user Checkbox.";
            return false;
        }
        narrator.finalizeTest("Successfully ticked the Tick Business unit verification and approval user Checkbox");
         pause(5000);
         
         //Tick the “Group Approval” checkbox
        if(!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.groupApporoval_Checkbox())){
            error = "Failed to wait for the Tick the “Group Approval” checkbox.";
            return false;
        }
        narrator.finalizeTest("Successfully viewed the Tick the “Group Approval” checkbox");
        
        //Scroll to element
        SeleniumDriverInstance.scrollToElement(Project_Management_PageObject.StrategicObjectives_tab());
        narrator.finalizeTest("The Project Approval, Project Financials and ECO2Man tabs are displayed.");
         pause(5000);
        //Save Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Project_Management_PageObject.saveBtn())) {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Project_Management_PageObject.saveBtn())) {
            error = "Failed to click on 'Save' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the save button.");

        pause(6000);


        return true;
    }
   

}
